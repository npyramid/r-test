const actions = require('./actions');

module.exports = {
  name: 'reader',

  dependencies: [
    'writer',
  ],

  actions,
};
