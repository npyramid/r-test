module.exports = {
  async upload(ctx) {
    const { params: readStream, meta: { filename }, broker } = ctx;

    const { filename: fileKey } = await broker.call('writer.save', { filename });

    const awaitForDrain = (key, chunk) => {
      setTimeout(async () => {
        const isBusy = await broker.call('writer.write', { filename: key, chunk });

        if (!isBusy) {
          return false;
        }

        return awaitForDrain(key, chunk);
      }, 10);
    };

    const uploadPromise = () => new Promise((resolve, reject) => {
      readStream
        .on('data', async (chunk) => {
          const isBusy = await broker.call('writer.write', { filename: fileKey, chunk });

          if (isBusy) {
            readStream.pause();

            awaitForDrain(fileKey, chunk);
            readStream.resume();
          }
        })
        .on('error', async (err) => {
          await broker.call('writer.clear', { filename: fileKey });
          reject(err);
        })
        .on('end', async () => {
          await broker.call('writer.end', { filename: fileKey });
          resolve();
        });
    });

    return uploadPromise()
      .then(() => ({ filename: fileKey }))
      .catch((err) => {
        throw err;
      });
  },
};
