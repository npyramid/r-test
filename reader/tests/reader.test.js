const path = require('path');
const fs = require('fs');
const { assert } = require('chai');
const broker = require('..');

const compareFiles = (firstFilePath, secondFilePath) => {
  const firstFile = fs.readFileSync(firstFilePath);
  const secondFile = fs.readFileSync(secondFilePath);
  return assert(firstFile.equals(secondFile), 'Files should be equal');
};

describe('Reader service', () => {
  let uploadedFilename = null;

  it('Should get upload file', async () => {
    const filename = 'file.dmg';
    const pathToFile = path.join(__dirname, 'files', filename);
    const fileStream = fs.createReadStream(pathToFile);
    const { filename: uploadedName } = await broker.call('reader.upload', fileStream, { meta: { filename } });
    assert(uploadedName);

    uploadedFilename = uploadedName;
  });

  it('Files must be equals', () => {
    const originalFilePath = path.join(__dirname, 'files', 'file.dmg');
    const uploadedFilePath = path.join(__dirname, '..', '..', 'writer', 'files', uploadedFilename);
    compareFiles(originalFilePath, uploadedFilePath);
  });

  it('Should upload file with same filename', async () => {
    const filename = 'file.dmg';
    const pathToFile = path.join(__dirname, 'files', filename);
    const fileStream = fs.createReadStream(pathToFile);
    const { filename: uploadedName } = await broker.call('reader.upload', fileStream, { meta: { filename } });
    assert(uploadedName);

    uploadedFilename = uploadedName;
  });

  it('Files must be equals', () => {
    const originalFilePath = path.join(__dirname, 'files', 'file.dmg');
    const uploadedFilePath = path.join(__dirname, '..', '..', 'writer', 'files', uploadedFilename);
    compareFiles(originalFilePath, uploadedFilePath);
  });
});
