const { ServiceBroker } = require('moleculer');
const brokerConfig = require('./config/brokerConfig');
const service = require('./service');

const broker = new ServiceBroker(brokerConfig);

broker.createService(service);

if (!module.parent) {
  broker.start().then(() => broker.logger.info('App started'));
}

module.exports = broker;
