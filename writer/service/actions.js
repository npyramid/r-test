const fs = require('fs');
const path = require('path');
const Promise = require('bluebird');
const randomString = require('randomstring');
const { Buffer } = require('buffer');

const readDir = Promise.promisify(fs.readdir);
const remove = Promise.promisify(fs.unlink);

const filesDirPath = path.join(__dirname, '..', 'files');

const streams = new Map();

module.exports = {
  save: {
    params: {
      filename: { type: 'string', min: 1 },
    },

    async handler(ctx) {
      const { params: { filename: originalFileName }, broker } = ctx;

      const filename = await broker.call('writer.getFilename', { filename: originalFileName });

      const filePath = path.join(filesDirPath, filename);

      const stream = fs.createWriteStream(filePath);
      streams.set(filename, stream);

      return { filename, stream };
    },
  },

  write: {
    params: {
      filename: { type: 'string', min: 1 },
      chunk: { type: 'any' },
    },

    handler(ctx) {
      const { params: { filename, chunk } } = ctx;
      const stream = streams.get(filename);
      const buffer = Buffer.from(chunk);
      return stream.write(buffer);
    },
  },

  end: {
    params: {
      filename: { type: 'string', min: 1 },
    },

    handler(ctx) {
      const { params: { filename } } = ctx;
      const stream = streams.get(filename);

      return stream.close((err) => {
        if (err) {
          streams.delete(filename);
          throw err;
        }

        return true;
      });
    },
  },

  clear: {
    params: {
      filename: { type: 'string', min: 1 },
    },

    async handler(ctx) {
      const { params: { filename }, broker } = ctx;

      await Promise.all([
        broker.call('writer.end', { filename }),
        broker.call('writer.remove', { filename }),
      ]);
    },
  },

  getFilename: {
    params: {
      filename: { type: 'string', min: 1 },
    },

    async handler(ctx) {
      const { params: { filename }, broker } = ctx;

      const filesList = await broker.call('writer.filesList');
      const isFileExists = filesList.includes(filename);

      if (!isFileExists) {
        return filename;
      }

      const parts = filename.split('.');
      const name = parts[0];
      const hash = randomString.generate({ length: 10, capitalization: 'lowercase' });
      const newName = `${name}-${hash}`;

      return [newName, ...parts.slice(1)].join('.');
    },
  },

  remove: {
    params: {
      filename: { type: 'string', min: 1 },
    },

    handler(ctx) {
      const { params: { filename } } = ctx;

      const filePath = path.join(filesDirPath, filename);

      return remove(filePath);
    },
  },

  filesList() {
    return readDir(filesDirPath);
  },
};
