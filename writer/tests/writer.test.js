const fs = require('fs');
const path = require('path');
const { assert } = require('chai');
const { ValidationError } = require('moleculer').Errors;
const broker = require('..');

const compareFiles = (firstFilePath, secondFilePath) => {
  const firstFile = fs.readFileSync(firstFilePath);
  const secondFile = fs.readFileSync(secondFilePath);
  return assert(firstFile.equals(secondFile), 'Files should be equal');
};

describe('Writer service', () => {
  describe('Validations', () => {
    it('Should get all validations errors from actions with it', () => {
      const promises = [
        broker.call('writer.save'),
        broker.call('writer.getFilename'),
        broker.call('writer.remove'),
        broker.call('writer.write'),
        broker.call('writer.end'),
        broker.call('writer.clear'),
      ];

      return Promise
        .allSettled(promises)
        .then((results) => {
          results.forEach((current) => {
            assert(current.reason);
            assert(current.reason instanceof ValidationError);
          });
        });
    });
  });

  describe('Main', () => {
    // remove all uploaded files
    before(async () => {
      const files = (await broker.call('writer.filesList')).filter((name) => name !== '.gitkeep');
      return Promise.all(files.map((filename) => broker.call('writer.remove', { filename })));
    });

    const name = 'file';
    const ext = 'dmg';
    const filename = [name, ext].join('.');

    it('Should get writable stream', async () => {
      const { stream, filename: receivedFilename } = await broker.call('writer.save', { filename });
      stream.close();
      assert.equal(filename, receivedFilename);
    });

    it('Should have new file', async () => {
      const files = await broker.call('writer.filesList');
      assert(files.includes(filename));
    });

    it('Should remove empty file', async () => {
      await broker.call('writer.remove', { filename });
      const files = await broker.call('writer.filesList');
      assert(!files.includes(filename));
    });

    it('Should save file', async () => {
      const { stream: writeStream } = await broker.call('writer.save', { filename });
      const filePath = path.join(__dirname, 'files', filename);
      const readStream = fs.createReadStream(filePath);
      const stream = readStream.pipe(writeStream);

      return new Promise((resolve) => {
        stream.on('close', () => {
          const uploadedFilePath = path.join(__dirname, '..', 'files', filename);
          compareFiles(filePath, uploadedFilePath);
          resolve();
        });
      });
    });

    context('Files with same name', () => {
      const filenameRegexp = /file-*/;

      it('Should save file with same name', async () => {
        const { stream: writeStream, filename: receivedFilename } = await broker.call('writer.save', { filename });
        const filePath = path.join(__dirname, 'files', filename);
        const readStream = fs.createReadStream(filePath);
        const stream = readStream.pipe(writeStream);

        assert(filenameRegexp.test(receivedFilename));

        return new Promise((resolve) => {
          stream.on('close', () => {
            resolve();
          });
        });
      });

      it('Should have file with hash', async () => {
        const list = await broker.call('writer.filesList');
        const target = list.find((file) => filenameRegexp.test(file));

        assert(target, 'File should be exist with mask ({name}-{hash}.{ext})');

        const originalFilePath = path.join(__dirname, 'files', filename);
        const uploadedFilePath = path.join(__dirname, '..', 'files', target);
        return compareFiles(originalFilePath, uploadedFilePath);
      });

      it('Should have file with original filename', async () => {
        const list = await broker.call('writer.filesList');
        assert(list.includes(filename));
      });
    });
  });
});
