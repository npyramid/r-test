const broker = require('..');

before(() => broker.start());

after((done) => {
  const exit = (code, message = '', error = '') => {
    if (code === 1) {
      broker.logger.error(message, error);
    } else {
      broker.logger.info(message);
    }

    process.exit(code);
  };

  const end = (err) => {
    if (err) {
      done(err);
      exit(1, 'Error occurred lowering app: ', err);
    }

    done();
    exit(0, 'App lowered successfully!');
  };

  broker
    .stop()
    .then(() => end())
    .catch(end);
});
